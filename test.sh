#!/bin/bash


echo "TESTING..."

i=1
while [ $i -lt 30 ]; do
    ok=$(mpirun -n $i src/laboratorio src/test.txt | diff - respuesta.txt)
    if ! [[ -z $ok ]]; then
        echo "Error en i=$i !!!"
        echo $ok
        break
    fi

    let i=i+1

done
