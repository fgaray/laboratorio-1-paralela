#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>


#include "Parser.hpp"



vector<Pais> parse(const string &archivo)
{
    
    ifstream fichero;
    vector<string> lineas;
    char buff[1024];
    
    fichero.open(archivo.c_str(), ios::binary);

    if(!fichero.is_open())
    {
        throw ErrorParse("El archivo no se puede abrir");
    }
    

    //leemos las lineas del archivo y las pasamos a un vector de strings
    while(!fichero.eof())
    {
            fichero.getline(buff, 1024);
    
            if(not string(buff).empty())
            {
                lineas.push_back(buff);
            }
    }
    
    return parse(lineas);
}


vector<Pais> parse(const vector<string> &lineas)
{
    int n_linea = 0;

    vector<string> nombre_paises;
    vector<vector<int> > probabilidades;
    vector<Pais> paises;

    //recorremos las lineas y convertimos sus valores a nombre de pais o a
    //entero segun corresponda
    for(vector<string>::const_iterator i = lineas.begin(); i != lineas.end(); ++i, n_linea++)
    {
        const string &linea = *i;


        if(n_linea < 16)
        {
            //es un nombre de pais
            nombre_paises.push_back(linea);
        }else{
            //es una linea que contiene las probabilidades

            const vector<int> &splited = toInt(splitAt(linea, ' '));

            probabilidades.push_back(splited);
        }
    }


    //revisamos que nos hayan pasado el mismo número de probabilidades que de
    //paises
    if(nombre_paises.size() != probabilidades.size())
    {
        throw ErrorFormato("No hay la misma cantidad de paises que de probabilidades");
    }

    //creamos los paises
    for(size_t i = 0; i < nombre_paises.size(); i++)
    {
        Pais p(nombre_paises.at(i), i);
        p.setProbabilidades(probabilidades.at(i));

        paises.push_back(p);
    }
    
    return paises;
}


vector<string> splitAt(const string &linea, char c)
{
    vector<string> splited;
    string cadena;

    for(string::const_iterator i = linea.begin(); i != linea.end(); ++i)
    {
        //encontramos el caracter que separa a las cadenas
        if(*i == c)
        {
            splited.push_back(cadena);
            cadena.erase(cadena.begin(), cadena.end());
        }else{
            cadena += *i;
        }
    }

    splited.push_back(cadena);

    return splited;
}


vector<int> toInt(const vector<string> &cadenas)
{
    int val;
    vector<int> vals;

    for(vector<string>::const_iterator i = cadenas.begin(); i != cadenas.end(); ++i)
    {
        (stringstream(*i) >> val) ? true : throw ErrorParse(":("); 
        vals.push_back(val);
    }

    return vals;
}


