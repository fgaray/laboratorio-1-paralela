#ifndef _H_RANGO_
#define _H_RANGO_

#include <cstdlib>
#include <string>
using namespace std;



/**Esta clase representa un rango. Un rango nos dice cuales son los paises que
 * debemos calcular su probabilidad de que llegen a la final.
 *
 * Los rangos incluyen los limites superiro e inferiror [bajo, alto]
 * */
class Rango{

    private:
        bool hay;

    public:
        size_t bajo, alto;

        Rango(size_t bajo, size_t alto);
        Rango();
        bool hayRango() const;


        string toString() const;
};


#endif /* end of include guard: _H_RANGO_ */
