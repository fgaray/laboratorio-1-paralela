#include <iostream>
#include <climits>
#include <iomanip>
using namespace std;


extern "C" {
#include <bsp.h>
}


#include "main.hpp"
#include "Parser.hpp"
#include "Pais.hpp"
#include "Calculo.hpp"
#include "SuperStep.hpp"




char *archivo = NULL;

int main(int argc, char *argv[])
{
    if(argc != 2)
    {
        if(argc < 2)
        {
            cerr << "Faltan argumentos!" << endl;
        }else{
            cerr << "Muchos argumentos!" << endl;
        }

        cerr << "USO: mpirun -n N ./laboratorio ARCHIVO" << endl;

        return -1;
    }

    archivo = argv[1];


    bsp_init(bsp_main, argc, argv);
    bsp_main();

    return 0;
}



void bsp_main()
{
    bsp_begin(bsp_nprocs());

    // pid parte del 0 hasta n - 1
    int pid = bsp_pid();
    int nprocs = bsp_nprocs();




    //que cada proceso lea el caso de entrada, asi no ahorramos el problema de
    //tener que enviar los datos ya parseados y tener que pensar bien como
    //enviar estructuras de c++ en bsp

    vector<Pais> paises;

    try{
         paises = parse(archivo);
    }catch(const ErrorParse &parse){
        cerr << "Error de parseo: " << parse.what() << endl;
        cerr << "En PID: " << pid << endl;
    }catch(const ErrorFormato &formato){
        cerr << "Error de formato: " << formato.what() << endl;
        cerr << "En PID: " << pid << endl;
    }


    //vemos en que rango debemos trabajar
    const Rango &rango = rangos(pid, nprocs, paises);

    //calculamos las respuestas de los paises que debiamos calcular
    const vector<double> &respuestas = superstep1(paises, rango);

    if(pid != 0)
    {

        //enviamos los datos al procesador 0
        enviar_datos(respuestas, pid);
        bsp_sync();
    }else{

        bsp_sync();

        //obtenemos las respuestas de los otros procesadores
        const vector<double> nrespuestas = respuestas_finales(respuestas, nprocs, paises);

        //mostramos las respuestas
        for(size_t i = 0; i < nrespuestas.size() && i < paises.size(); i++)
        {
            cout << left << setw(15) << paises.at(i).getNombre() << right << "p=" << setprecision(3) << nrespuestas.at(i) << "%" << endl;
        }
    }

    bsp_sync();
    bsp_end();
}
