#ifndef _H_PARSE_
#define _H_PARSE_

#include <string>
using namespace std;


#include "Pais.hpp"

/** Recibe un nombre de archivo y devuelve los paises encontrados en el.
 *
 * @param archivo El nombre del archivo del cual leer el caso
 *
 * @return Un vector de paises segun el nombre de archivo dado
 * */
vector<Pais> parse(const string &archivo);


/**A partir de las lineas leidas de un archivo, recoje la informacion para ser
 * pasada a un objeto Pais. Como en las lineas pueden haber muchos paises se
 * pasa un vector de paises
 *
 * @param lineas Un vector de strings donde cada string es una linea hasta el
 * 
 * @return Un vector de paises segun las lineas dadas
 * */
vector<Pais> parse(const vector<string> &lineas);


/**Separa un string entre los caracteres dados. Si el caracter dado es el
 * espacio ' ' entonces: hola mundo lo separa en [hola, mundo]
 *
 * @param linea La linea a separar en tokens
 * @param c El caracter delimitador entre los tokens
 *
 * @return retorna un vector donde cada elemento es un string de las separadas
 * por el caracter c. Un vector de tokens
 * */
vector<string> splitAt(const string &linea, char c);



/**Convierte las cadenas de un vector a enteros
 *
 * @param cadenas Un vector de strings a ser convertidas en enteros 
 *
 * @return Un vector de int con los strings convertidos*/
vector<int> toInt(const vector<string> &cadenas);


/**En caso de un error al hacer el parse se lanza esta excepción. Se puede
 * enviar un mensaje indicando lo que paso al usuario con el método what*/
class ErrorParse{
        public:
                ErrorParse(const string m="Error")
                {
                    this->msg = m;
                }
                string what() const
                {
                    return this->msg;
                }
        private:
                string msg;
};


/**En caso de un error al hacer el parse se lanza esta excepción. Esta excepcion
 * informa sobre un error en el formato del documento como por ejemplo una
 * cantidad incorrecta de paises y probabilidades */
class ErrorFormato{
        public:
                ErrorFormato(const string m="Error")
                {
                    this->msg = m;
                }
                string what() const
                {
                    return this->msg;
                }
        private:
                string msg;
};

#endif /* end of include guard: _H_PARSE_ */
