#include <iostream>
#include <climits>
#include <algorithm>


#include "SuperStep.hpp"
#include "Calculo.hpp"
#include "Respuesta.hpp"


const Rango rangos(int pid, int nprocs, const vector<Pais> &paises)
{
    //ok, somos el procesador "pid", debemos ver que pais nos toca calcular

    //revisamos si hay más procesos que paises a calcular
    if(nprocs > paises.size())
    {
       if(pid + 1 > paises.size())
       {
           return Rango();
       }

       nprocs = paises.size();
    }

    size_t n_paises = paises.size() / nprocs;
    int adicional = 0;


    // ¿somos el ultimo?
    if(pid + 1 == nprocs)
    {
        //somos el ultimo proceso, por lo tanto si el numero de paises no es
        //multiplo de la cantidad de procesos, hay que procesar más paises
        
        //debemos procesar lo que nos corresponde + resto
        adicional = paises.size() % nprocs;
    }

    // se resta -1 en el limite superior ya que no hay que hacer los paises que
    // vienen despues
    return Rango(pid * n_paises, pid * n_paises + n_paises + adicional - 1);
}


const vector<Rango> todos_rangos(int nprocs, const vector<Pais> &paises)
{
    vector<Rango> todos;

    for(int i = 0; i < nprocs; i++)
    {
        todos.push_back(rangos(i, nprocs, paises));
    }

    return todos;
}

const vector<double> superstep1(const vector<Pais> &paises, const Rango &rango)
{
    vector<double> respuestas;

    if(rango.hayRango())
    {
        for(size_t i = rango.bajo; i <= rango.alto; i++)
        {
            const Pais &pais = paises.at(i);
            respuestas.push_back(calcular(pais, paises, paises.size()) * 100.0);
        }
    }

    return respuestas;
}



void enviar_datos(const vector<double> &respuestas, int pid)
{
    //necesitamos un entero para guardar el procesador del cual estamos enviando
    //las respuestas. El resto son las respuestas en si las cuales son doubles
    int mem_requerida = sizeof(int) + sizeof(double) * respuestas.size() + 1;

    int TAG = 0;

    void *datos = malloc(mem_requerida);
    double *double_mem = NULL;

    // guardamos el pid en la primera posicion de nuestro arreglo
    *((int*)datos) = pid;


    /*
     * Pointer magic
     *                                 .
     *                       /^\     .
     *                  /\   "V"
     *                 /__\   I      O  o
     *                //..\\  I     .
     *                \].`[/  I
     *                /l\/j\  (]    .  O
     *               /. ~~ ,\/I          .
     *               \\L__j^\/I       o
     *                \/--v}  I     o   .
     *                |    |  I   _________
     *                |    |  I c(`       ')o
     *                |    l  I   \.     ,/      
     *              _/j  L l\_!  _//^---^\\_
     *           ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     *                     ->Wizard<-
     *
     * Un poco de explicación: Tenemos datos que es un puntero void a una área
     * de la memoria en la cual debemos copiar los datos. Ya guardamos un int en
     * los primeros 4 bytes y tenemos el resto para guardar doubles. Asi que
     * necesitamos saber donde comenzar a escribir los doubles. Primero
     * casteamos datos de void* a int*, nos saltamos el int que ya escribirmos
     * (+1) y luego volvemos a castear el puntero pero a double.
     *
     */
    double_mem = (double*)((int*)datos + 1);


    // copiamos los datos de las respuestas en la memoria correspondiente
    for(size_t i = 0; i < respuestas.size(); i++)
    {
        double_mem[i] = respuestas.at(i);
    }
    
    bsp_send(0, &TAG, datos, mem_requerida);

    free(datos);
}


const vector<double> respuestas_finales(const vector<double> &respuestas_propias, int nprocs, const vector<Pais> &paises)
{

    vector<double> respuestas(respuestas_propias);


    //obtenemos el numero de mensajes y el total de bytes
    int numero_mensajes, total_bytes;
    bsp_qsize(&numero_mensajes, &total_bytes);


    //necesitamos saber los rangos que calcularon los procesadores para saber
    //cuando doubles tiene cada uno
    const vector<Rango> &rangos = todos_rangos(nprocs, paises);


    for(size_t i = 1; i < rangos.size(); i++)
    {
        void *recivido = malloc(total_bytes);

        //ok, tenemos el mensaje recibido
        bsp_move(recivido, total_bytes);

        //
        const Rango &rango = rangos.at(i);

        //ok, sabemos cuantos paises nos deberia haber mandado el otro
        //procesador

        size_t numero_respuestas = rango.alto - rango.bajo + 1;

        int n_procesador = 0;
        int *numero = ((int*)recivido);
        n_procesador = numero[0];

        vector<double> resps;

        double *doubles_enviados = (double*)((int*)recivido + 1);

        //hay que copiar los doubles al vector respuestas
        for(size_t j = 0; j < numero_respuestas; j++)
        {
            double resp = doubles_enviados[j];
            respuestas.push_back(resp);
        }


        free(recivido);
    }

    return respuestas;
}
