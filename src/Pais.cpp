#include "Pais.hpp"


Pais::Pais(string nombre, size_t id)
{
    this->nombre = nombre;
    this->id = id;
}


size_t Pais::getId() const
{
    return this->id;
}

string Pais::getNombre() const
{
    return this->nombre;
}

void Pais::setProbabilidades(const vector<int> &probs)
{
    this->probabilidades.assign(probs.begin(), probs.end());
}


int Pais::probabilidadGaneA(size_t id) const
{
    return this->probabilidades.at(id);
}


int Pais::probabilidadGaneA(const Pais &p) const
{
    return this->probabilidadGaneA(p.getId());
}
