#ifndef _H_CALCULO_
#define _H_CALCULO_

#include <vector>
using namespace std;


#include "Pais.hpp"


/**Calcula la probabilidad de que un pais gane la final de la copa
 *
 * @param pais El pais que se quiere obtener la probabilidad de ganar la final
 * @param paises Un vector de los paises del torneo
 * @param largo Un entero que indica el largo del conjunto de paises a evaluar, debe ser potencia de dos
 *
 * @return La probabilidad de que el pais 'pais' gane la final
 * */
double calcular(const Pais &pais, const vector<Pais> &paises, size_t largo);

/**Verifica si un pais pertenece a una rama izquierda dependiendo de la fase en que se encuentre
 * 
 * @param pais El pais que se quiere verificar
 * @param largoNivel Un entero que indica en que fase se encuentra el pais
 * @param paises Un vector de paises del torneo
 * 
 * @return verdadero si el pais pertenece a una rama izquierda, falso si no
 * */
bool esRamaIzquierda(const Pais &pais, size_t largoNivel, const vector<Pais> &paises);

/**Calcula el numero de la rama a la cual pertecene un pais 
 *
 * @param pais El pais al cual se quiere calcular el numero de rama
 * @param largoNivel Un entero que indica en que fase se encuentra el pais
 * @param paises Un vector de los paises del torneo
 *
 * @return El numero de rama a la cual pertenece el pais
 * */

size_t numRama(const Pais &pais, size_t largoNivel, const vector<Pais> &paises);

/**Calcula la probabilidad de que un pais que se encuentra en una rama derecha le gane a todos los posibles oponentes de cierta fase del torneo
 *
 * @param pais El pais al cual se quiere buscar la probabilidad
 * @param total Entero que indica el largo 
 * @param paises El vector de los paises del torneo
 *
 * @return la probabilidad de que el pais le gane a sus oponentes
 * */
double sumatoriaMayor(const Pais &pais, size_t total, const vector<Pais> &paises);

/**Calcula la probabilidad de que un pais que se encuentra en una rama izquierda le gane a todos los posibles oponentes de cierta fase del torneo
 *
 * @param pais El pais al cual se quiere buscar la probabilidad
 * @param total Entero que indica el largo 
 * @param paises El vector de los paises del torneo
 *
 * @return la probabilidad de que el pais le gane a sus oponentes
 * */
double sumatoriaMenor(const Pais &pais, size_t total, const vector<Pais> &paises);


#endif /* end of include guard: _H_CALCULO_ */
