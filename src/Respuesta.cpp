#include "Respuesta.hpp"




Respuesta::Respuesta(int pid, vector<double> respuestas)
{
    this->pid = pid;
    this->respuestas = respuestas;
}


bool Respuesta::operator<(const Respuesta otro) const
{
    return otro.pid > this->pid;
}
