#include "Rango.hpp"
#include <iostream>
#include <sstream>

Rango::Rango(size_t bajo, size_t alto)
{
    this->bajo = bajo;
    this->alto = alto;
    this->hay = true;
}


Rango::Rango()
{
    this->hay = false;
}


bool Rango::hayRango() const
{
    return this->hay;
}


string Rango::toString() const
{
    stringstream s;

    if(this->hay)
    {

        s << "[" << this->bajo;
        s << ", ";
        s << this->alto << "]";
    }else{
        s << "[]";
    }

    return s.str();
}
