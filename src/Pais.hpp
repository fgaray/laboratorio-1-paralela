#ifndef _H_PAIS_
#define _H_PAIS_

#include <vector>
#include <string>
using namespace std;


/**Esta clase representa a un pais con las probabilidades de que le gane a los
 * otros paises*/
class Pais{

    private:

        
        //id del pais
        size_t id;

        //nombre del pais
        string nombre;

        //las probabilidades de que le gane a otro pais n
        vector<int> probabilidades;


    public:

        /**Constructor
         *
         * @param nombre El nombre del pais
         * @param id El id de este pais usado para ver las probabilidades
         *
         * */
        Pais(string nombre, size_t id);

        /**Retorna el id de este pais*/
        size_t getId() const;

        /**Retorna el nombre de este pais*/
        string getNombre() const;

        /**Agrega las probabilidades de que este pais de gane al pais N
         *
         * @param probs Las probabilidades*/
        void setProbabilidades(const vector<int> &probs);

        /**Retorna la probabilidad de que este pais le gane al pais id
         *
         * @param id El id del pais que va a perder
         *
         * @return La probabilidad de que este pais le gane al pais del id*/
        int probabilidadGaneA(size_t id) const;


        int probabilidadGaneA(const Pais &p) const;
};



#endif /* end of include guard: _H_PAIS_ */
