#ifndef _H_SUPERSTEP_
#define _H_SUPERSTEP_

#include <vector>
using namespace std;

#include <malloc.h>

extern "C" {
#include <bsp.h>
}



#include "Pais.hpp"
#include "Rango.hpp"




/**Calcula el rango que le corresponde al procesador
 *
 * @param pid El pid del cual queremos saber el rango en el cual se deben
 * calcular las probabilidades
 *
 * @param nprocs El total de procesos a ser ejecutados por mpirun
 *
 * @param paises Un vector que contenga todos los paises del caso
 *
 * @return Un rango en el cual este pid debe calcular las probabilidades
 *
 * */
const Rango rangos(int pid, int nprocs, const vector<Pais> &paises);


/**El superstep 1. Calcula las probabilidades en el rango de paises dado
 *
 * @param paises Un vector de los paises que hay en el caso
 * @param rango El rango en el cual se van a calcular las probabilidades
 *
 * @return Un vector que contiene las probabilidades de que cada equipo en el
 * rango llegue a la final
 *  */
const vector<double> superstep1(const vector<Pais> &paises, const Rango &rango);


/**Envia las respuestas al procesador 0 para que una todas las respuestas y
 * presente la solución final de forma ordenada
 *
 * @param respuestas Un vector con las respuestas a enviar al procesador 0
 * @prarm pid El pid de este procesador para que el procesador 0 sepa como
 * ordenar los datos
 * */
void enviar_datos(const vector<double> &respuestas, int pid);


/**Obtiene las respuestas finales desde los otros procesadores, los mezcla (con
 * las respuestas propias y entre las enviadas) y devuelve un vector con todas
 * ellas ordenadas para que puedan ser mostradas
 *
 * @param respuestas_propias Las respuestas calculadas en este procesador
 * @param nprocs El numero de procesos lanzado por mpirun
 * @param paises Un vector con todos los paises que hay en el caso a ser
 * analizado
 *
 * @return Un vector con todas las respuestas unidas. El vector esta ordenado y
 * se debe imprimir solamente
 *
 * */
const vector<double> respuestas_finales(const vector<double> &respuestas_propias, int nprocs, const vector<Pais> &paises);


/**Calcula todos los rangos que se va a usar en esta ejecución segun el número
 * de procesos lanzados
 *
 * @param nprocs El número de procesos creados al ejecutar mpirun
 * @param paises Un vector que contiene todos los paises del caso
 *
 * @return un vector con todos los rangos que corresponden a los procesos
 * */
const vector<Rango> todos_rangos(int nprocs, const vector<Pais> &paises);


#endif /* end of include guard: _H_SUPERSTEP_ */
