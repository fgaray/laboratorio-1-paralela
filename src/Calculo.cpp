#include "Calculo.hpp"
#include <iostream>


double calcular(const Pais &pais, const vector<Pais> &paises, size_t largo)
{
    // si largo del conjunto de paises de la fase es el minimo
	if(largo == 2)
    {
        // cuando el pais tiene ide par
        if(pais.getId() % 2 == 0)
        {
			//retorna la probabilidad de que el pais 'pais' le gane a su oponente en octavos de final
            return pais.probabilidadGaneA( pais.getId() + 1 ) / 100.0;

        }else{

			//retorna la probabilidad de que el pais 'pais' le gane a su oponente en octavos de final
            return pais.probabilidadGaneA( pais.getId() - 1 ) / 100.0; 
        }
    }

	//Si el pais pertecene a una rama izquirda
	if(esRamaIzquierda(pais, largo / 2, paises))
    {   
		//retorna probabilidad de que el pais 'pais' llegue a una fase por la probabilidad de tal pais le gane a sus oponenetes en tal fase
        return calcular(pais, paises, largo / 2) * sumatoriaMenor(pais, largo / 2, paises);

    }else{
       
		//retorna probabilidad de que el pais 'pais' llegue a una fase por la probabilidad de tal pais le gane a sus oponenetes en tal fase
        return calcular(pais, paises, largo / 2) * sumatoriaMayor(pais, largo / 2, paises);
    }
}

double sumatoriaMenor(const Pais &pais, size_t total, const vector<Pais> &paises)
{
    double valor = 0;
	size_t numeroRama = numRama(pais, total, paises); //se calcula el numero de rama que pertenece el pais 'pais'

	//itera tantas veces como oponentes posibles tenga el pais 'pais' en una fase determinada
	//calculando la probabilidad de que les gane a tales oponentes
	for(size_t i = 0; i < total; i++)
    {

		//toma como oponentes a los paises que componen la rama derecha contigua a la rama izquierda que pertenece el pais 'pais'
        valor += pais.probabilidadGaneA(numeroRama*total +  i) / 100.0 * calcular(paises.at(numeroRama * total + i), paises, total);
    }
    return valor;
}

double sumatoriaMayor(const Pais &pais, size_t total, const vector<Pais> &paises)
{
    double valor = 0;
    size_t numeroRama = numRama(pais, total, paises) - 1; //se calcula el numero de rama que pertenece el pais 'pais'
  
	//itera tantas veces como oponentes posibles tenga el pais 'pais' en una fase determinada
	//calculando la probabilidad de que les gane a tales oponentes                              		
	for(size_t i = 1; i <= total; i++)
   	{    
		//toma como oponentes a los paises que componen la rama izquirda anterior a la rama derecha que pertenece el pais 'pais'
        valor += ( pais.probabilidadGaneA(numeroRama*total - i) / 100.0 ) *calcular(paises.at(numeroRama*total - i ), paises, total);
    }

    return valor;
}


bool esRamaIzquierda(const Pais &pais, size_t largoNivel, const vector<Pais> &paises){

	size_t ramasTotales = paises.size()/largoNivel; //numero de ramas que hay de porte largoNivel segun el conjunto total de paises
	size_t ramasIzq = ramasTotales/2; //numero de ramas izquierdas en la fase
	int esIzq = 0;
	size_t cont = 0;
	int mul = 0;
	
	//itera tantas ramas izquierdas hayan en tal fase del torneo
	while(cont < ramasIzq)
	{
		//verifica si el pais tiene un ide que pertenezca a los rangos de las ramas existentes
		if((pais.getId() >= mul * largoNivel) && (pais.getId() < ((mul + 1) * largoNivel)))
		{
			esIzq = true;
		}
		
		mul += 2;
		cont++;
	}

	return esIzq;
}

size_t numRama(const Pais &pais, size_t largoNivel, const vector<Pais> &paises){

	size_t ramasTotales = paises.size() / largoNivel; //numero de ramas que hay de porte largoNivel segun el conjunto total de paises
	size_t numRama = 0;
	size_t cont = 0;
	int mul = 0;

	//itera tantas ramas hayan (izquierdas mas derechas) en tal fase del torneo
	while(cont < ramasTotales)
	{
		//se busca segun los rangos de tal fase de torneo, a que rama pertenece el ide del pais 'pais'
		if(pais.getId() >= mul * largoNivel && pais.getId() < (mul + 1) * largoNivel)	
		{
			numRama = mul + 1; //ya que se interpreta la numeracion de las ramas desde uno 
		}

		mul++;
		cont++;
	}

	return numRama;
}
