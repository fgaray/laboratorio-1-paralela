#ifndef _H_RESPUESTA_
#define _H_RESPUESTA_

#include <vector>
using namespace std;


/**Esta clase nos ayuda a relacionar las respuestas (probabilidades) con el
 * numero de procedador del cual salieron. Por lo tanto es posible ordenar las
 * respuestas en realcion al pid. */
class Respuesta{

    public:

        int pid;
        vector<double> respuestas;

        Respuesta(int pid, vector<double> respuestas);


        bool operator<(const Respuesta otro) const;
};


#endif /* end of include guard: _H_RESPUESTA_ */
