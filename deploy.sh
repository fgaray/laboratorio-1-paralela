#!/bin/bash


# Vemos si existe ya un lab.tar
if [ -f lab.tar ]; then
    rm lab.tar
fi

# Vemos si existe ya un lab.tar.bz2
if [ -f lab.tar.bz2 ]; then
    rm lab.tar.bz2
fi







# Guaramos este directorio
tar --exclude-from=.gitignore  -cf ../lab.tar .
bzip2 ../lab.tar
mv ../lab.tar.bz2 ./
scp lab.tar.bz2 cp17@158.170.35.8:./


if [ -f lab.tar.bz2 ]; then
    rm lab.tar.bz2
fi
