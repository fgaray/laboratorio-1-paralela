# Creado por Felipe Garay


find_path(BSP_INCLUDE_DIR bsp.h PATHS /usr/local/bsponmpi/include)
if(BSP_INCLUDE_DIR)
    SET(BSP_FOUND true)
    SET(BSP_LIBS /usr/local/bsponmpi/lib/libbsponmpi.so)
else()
    find_path(BSP_INCLUDE_DIR bsp.h PATHS "$ENV{HOME}/bsponmpi/include")
    if(BSP_INCLUDE_DIR)
        # bsp se encuentra en la carpeta home, asi que hay que linkear con home
        SET(BSP_FOUND true)
        SET(BSP_LIBS "$ENV{HOME}/bsponmpi/lib/libbsponmpi.so")
    else()
        SET(BSP_FOUND false)
    endif()
endif()
