 Compilación

## Requisitos para compilar

- cmake
- bsponmpi instalado en /usr/local/bsponmpi/ o en /home/USUARIO/bsponmpi.

## Compilación

En el directorio lab1/ escriba
  
  cmake .

Esto va a buscar bsponmpi en /usr/local y en el directorio HOME. Si todo sale bien
entonces va a crear un Makefile en el directorio actual. Para compilar ejecute:

  make

Una vez terminado este proceso va a encontrar un programa llamado "laboratorio"
en la carpeta src/
    

# Ejecución

Ejecute:

  mpirun -n N ./src/laboratorio src/test.txt


## Pruebas

Puede ejecutar el archivo de pruebas "test.sh" que va a comprobar con 1, 2, 3,
.., 20 procesos para ver que todos den la respuesta esperada.




# Directorio

Se incluye un archivo de prueba (sacado del enunciado del laboratorio) llamado
test.txt que se encuentra en la carpeta src/

Tambien hay un archivo con las respuestas esperadas llamado respuesta.txt para
el archvio dado en el enunciado




# Resumen enunciado

- Un campeonato de futbol esta llegando al final con 16 paises.
- El ganador esta dado segun un diagrama y en este orden:

  * Brazil
  * Chile

  * Nigeria
  * Denmark

  * Holland
  * Yugoslavia

  * Argentina
  * England

  * Italy
  * Norway
  
  * France
  * Paraguay

  * Germany
  * Mexico

  * Romania
  * Croatia

- Para cada posible partido entre los 16 paises se dan la probabilidad de que el
  equipo A gane a B.
- En base a esto se puede calcular efectivamente la probabilidad de que un pais
  gane la copa mundial.




## Entrada

- El archivo de entrada contiene solo un caso
- Las primeras 16 líneas son los nombres de los 16 paises en orden segun el
  diagrama.
- Luego una matriz de 16x16 enteros donde cada elemento pij es la probabilidad
  en porcentaje de que el pais i derrote al pais j. Los partidos no terminan con
  empates.


## Salida

- 16 líneas de la forma: PAIS p=P.PP% donde PAIS el el nombre del pais y P.PP el
  porcentaje de que gane la copa mundial el partido.
